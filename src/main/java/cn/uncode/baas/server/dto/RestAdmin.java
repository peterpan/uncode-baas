package cn.uncode.baas.server.dto;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cn.uncode.dal.utils.JsonUtils;

public class RestAdmin implements Serializable, Comparable<RestAdmin> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2062016870348149714L;
	
	public static final String ID = "id";
	public static final String BUCKET = "bucket";
	public static final String TABLE_NAME = "tableName";
	public static final String NAME = "name";
	public static final String ALIAS_NAME = "aliasName";
	public static final String TYPE = "type";
	public static final String MIN_LENGTH = "minLength";
	public static final String MAX_LENGTH = "maxLength";
	public static final String REQUEST = "request";
	public static final String DISPLAY_TYPE = "displayType";
	public static final String DISPLAY_VALUE = "displayValue";
	public static final String SORT = "sort";
	public static final String SEARCH_TYPE = "searchType";
	public static final String VALIDATE = "validate";
	public static final String FORM_TYPE = "formType";
	public static final String FORM_VALUE = "formValue";
	
	private int id;
	private String bucket;
	private String tableName;
	private String name;
	private String aliasName;
	private int type;
	private int minLength;
	private int maxLength;
	private boolean request;
	private int displayType;
	private String displayValue;
	private Map<String, String> displayValueMap;
	private int sort;
	private int searchType;
	private String validate;
	private int formType;
	private String formValue;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAliasName() {
		return aliasName;
	}
	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getMinLength() {
		return minLength;
	}
	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}
	public int getMaxLength() {
		return maxLength;
	}
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}
	public boolean isRequest() {
		return request;
	}
	public void setRequest(boolean request) {
		this.request = request;
	}
	public int getDisplayType() {
		return displayType;
	}
	public void setDisplayType(int displayType) {
		this.displayType = displayType;
	}
	public String getDisplayValue() {
		return displayValue;
	}
	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
		if(StringUtils.isNotEmpty(displayValue)){
			this.displayValueMap = JsonUtils.fromJson(displayValue, Map.class);
		}
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public int getSearchType() {
		return searchType;
	}
	public void setSearchType(int searchType) {
		this.searchType = searchType;
	}
	public String getValidate() {
		return validate;
	}
	public void setValidate(String validate) {
		this.validate = validate;
	}
	public int getFormType() {
		return formType;
	}
	public void setFormType(int formType) {
		this.formType = formType;
	}
	public String getFormValue() {
		return formValue;
	}
	public void setFormValue(String formValue) {
		this.formValue = formValue;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	
	public Map<String, String> getDisplayValueMap() {
		return displayValueMap;
	}

	@Override
	public int compareTo(RestAdmin obj) {
		if (this.getSort() > obj.getSort()) {   
            return 1;
        } else if (this.getSort() < obj.getSort()) {   
            return -1;
        } else {   
            return 0;
        }
	}
	

}
