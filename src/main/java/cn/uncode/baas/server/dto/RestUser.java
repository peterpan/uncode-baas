package cn.uncode.baas.server.dto;

import java.io.Serializable;

public class RestUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8539801603569278105L;
	
	public static final String ID = "id";
	public static final String BUCKET = "bucket";
	public static final String TABLE_NAME = "tableName";
	public static final String USERNAME_FIELD = "usernameField";
	public static final String PASSWORD_FIELD = "passwordField";
	public static final String STATUS_FIELD = "statusField";
	public static final String EMAIL_AUTH = "emailAuth";
	public static final String EMAIL_FIELD = "emailField";
	public static final String DEFAULT_GROUP = "defaultGroup";
	public static final String MOBILE_FIELD = "mobileField";
	
	private int id;
	private String bucket;
	private String tableName;
	private String usernameField;
	private String passwordField;
	private String statusField;
	private String emailField;
	private String mobileField;
	private int emailAuth;
	private String defaultGroup;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getUsernameField() {
		return usernameField;
	}
	public void setUsernameField(String usernameField) {
		this.usernameField = usernameField;
	}
	public String getPasswordField() {
		return passwordField;
	}
	public void setPasswordField(String passwordField) {
		this.passwordField = passwordField;
	}
	public String getStatusField() {
		return statusField;
	}
	public void setStatusField(String statusField) {
		this.statusField = statusField;
	}
	public int getEmailAuth() {
		return emailAuth;
	}
	public void setEmailAuth(int emailAuth) {
		this.emailAuth = emailAuth;
	}
	
	public String getEmailField() {
		return emailField;
	}
	public void setEmailField(String emailField) {
		this.emailField = emailField;
	}
	public String getMobileField() {
		return mobileField;
	}
	public void setMobileField(String mobileField) {
		this.mobileField = mobileField;
	}
	public String getDefaultGroup() {
		return defaultGroup;
	}
	public void setDefaultGroup(String defaultGroup) {
		this.defaultGroup = defaultGroup;
	}
	
	
	
	

}
