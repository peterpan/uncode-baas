package cn.uncode.baas.server.resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import cn.uncode.dal.cache.CacheManager;
import cn.uncode.baas.server.cache.SystemCache;
import cn.uncode.baas.server.dto.RestApp;
import cn.uncode.baas.server.service.IGenericService;
import cn.uncode.baas.server.service.IResterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 通用rest接口
 * 
 * @author ywj
 * 
 */
@Controller
@RequestMapping("/cache")
public class CacheResource extends BaseResource {

    private static final Logger LOG = Logger.getLogger(CacheResource.class);

    @Autowired
    private IGenericService genericService;

    @Autowired
    private IResterService resterService;

    @Autowired
    private CacheManager cacheManager;

    @RequestMapping(value = "/method/{bucket}/{restName}/{option}/{version}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> clearRestMethodCache(@PathVariable("bucket") String bucket,
            @PathVariable("restName") String restName, @PathVariable("option") String option,
            @PathVariable("version") String version) {
        genericService.clearExecuterCache(bucket, restName, option, version);
        SystemCache.clearRestMethod(bucket, restName, option, version);
        SystemCache.clearScriptEngine(bucket, restName, option, version);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/app/{bucket}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> clearRestAppCache(@PathVariable("bucket") String bucket) {
        SystemCache.clearRestApp(bucket);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/user/{bucket}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> clearRestUserCache(@PathVariable("bucket") String bucket) {
        SystemCache.clearRestUser(bucket);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/table/{bucket}/{tableName}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> clearRestTableCache(@PathVariable("bucket") String bucket,
            @PathVariable("tableName") String tableName) {
        SystemCache.clearRestTable(bucket, tableName);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/admin/{bucket}/{tableName}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> clearRestAdminCache(@PathVariable("bucket") String bucket,
            @PathVariable("tableName") String tableName) {
        SystemCache.clearRestAdmin(bucket, tableName);
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/daltable/{bucket}/{name}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> clearTableCache(@PathVariable("bucket") String bucket,
            @PathVariable("name") String name) {
        RestApp restApp = SystemCache.getRestApp(bucket);
        if (restApp != null && StringUtils.isNotBlank(restApp.getDbURL())) {
            int end = restApp.getDbURL().lastIndexOf("?");
            int start = restApp.getDbURL().lastIndexOf("/") + 1;
            if (end == -1) {
                end = restApp.getDbURL().length();
            }
            String database = restApp.getDbURL().substring(start, end);
            genericService.clearDALCache(database, name);
        }
        return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
    }

}
