package cn.uncode.baas.server.cache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.script.ScriptEngine;

import cn.uncode.baas.server.dto.RestAdmin;
import cn.uncode.baas.server.dto.RestApp;
import cn.uncode.baas.server.dto.RestMethod;
import cn.uncode.baas.server.dto.RestTable;
import cn.uncode.baas.server.dto.RestUser;
import cn.uncode.baas.server.service.IResterService;
import cn.uncode.baas.server.utils.WebApplicationContextUtil;

public class SystemCache {

    private static final ConcurrentMap<String, Object> cache = new ConcurrentHashMap<String, Object>();

    private static final String REST_APP_CACHE_PREFIX = "_restapp_";
    private static final String REST_APP_USER_PREFIX = "_restuser_";
    private static final String REST_APP_METHOD_PREFIX = "_restmethod_";
    private static final String REST_SCRIPT_ENGINE = "_scriptEngine_";
    private static final String REST_TABLES = "_restTable_";
    private static final String REST_ADMIN = "_restAdmin_";
    private static final String REST_ADMIN_FIELDS = "_restAdmin_fields_";

    public static int getSize() {
        return cache.size();
    }

    public static void setRestApp(RestApp value) {
        cache.put(REST_APP_CACHE_PREFIX + value.getBucket(), value);
    }

    public static RestApp getRestApp(String bucket) {
        RestApp restApp = (RestApp) cache.get(REST_APP_CACHE_PREFIX + bucket);
        if (restApp == null) {
            IResterService resterService = WebApplicationContextUtil.getBean("resterService", IResterService.class);
            restApp = resterService.loadRestApp(bucket);
            if (restApp != null) {
                cache.put(REST_APP_CACHE_PREFIX + bucket, restApp);
            }
        }
        return restApp;
    }

    public static RestApp getRestApp4Data(String bucket) {
        return getRestApp(bucket);
    }

    public static void clearRestApp(String bucket) {
        cache.remove(REST_APP_CACHE_PREFIX + bucket);
    }

    public static void setRestTable(RestTable value) {
        cache.put(REST_TABLES + value.getKey(), value);
    }

    public static RestTable getRestTable(String bucket, String name) {
        String key = bucket + "_" + name;
        RestTable restTable = (RestTable) cache.get(REST_TABLES + key);
        if (restTable == null) {
            IResterService resterService = WebApplicationContextUtil.getBean("resterService", IResterService.class);
            restTable = resterService.loadRestTable(bucket, name);
            if (restTable != null) {
                cache.put(REST_TABLES + key, restTable);
            }
        }
        return restTable;
    }

    public static void clearRestTable(String bucket, String name) {
        String key = bucket + "_" + name;
        cache.remove(REST_TABLES + key);
    }

    public static void setRestMethod(RestMethod value) {
        cache.put(REST_APP_METHOD_PREFIX + value.getKey(), value);
    }

    public static RestMethod getRestMethod(String bucket, String name, String option, String version) {
        String key = "rester_" + bucket + "_" + name + "_" + option + "_" + version;
        Object restMethodObj = cache.get(REST_APP_METHOD_PREFIX + key);
        RestMethod restMethod = null;
        if (restMethodObj == null) {
            IResterService resterService = WebApplicationContextUtil.getBean("resterService", IResterService.class);
            restMethod = resterService.loadRestMethod(bucket, name, option, version);
            if (restMethod != null) {
                cache.put(REST_APP_METHOD_PREFIX + key, restMethod);
            }
        } else {
            restMethod = (RestMethod) restMethodObj;
        }
        return restMethod;
    }

    public static void clearRestMethod(String bucket, String name, String option, String version) {
        String key = "rester_" + bucket + "_" + name + "_" + option + "_" + version;
        cache.remove(REST_APP_METHOD_PREFIX + key);
    }

    public static void setRestUser(RestUser value) {
        cache.put(REST_APP_USER_PREFIX + value.getBucket(), value);
    }

    public static void setRestAdmin(String bucket, String table, List<RestAdmin> value) {
        if (value != null && !value.isEmpty()) {
            cache.put(REST_ADMIN + bucket + "_" + table, value);
        }
    }

    public static List<RestAdmin> getRestAdmin(String bucket, String table) {
        List<RestAdmin> admins = (List<RestAdmin>) cache.get(REST_ADMIN + bucket + "_" + table);
        if (admins == null || admins.isEmpty()) {
            IResterService resterService = WebApplicationContextUtil.getBean("resterService", IResterService.class);
            admins = resterService.loadRestAdmin(bucket, table);
            if (admins != null && !admins.isEmpty()) {
                cache.put(REST_ADMIN + bucket + "_" + table, admins);
            }
        }
        Collections.sort(admins);
        return admins;
    }

    public static void clearRestAdmin(String bucket, String table) {
        cache.remove(REST_ADMIN + bucket + "_" + table);
        cache.remove(REST_ADMIN_FIELDS + bucket + "_" + table);
    }

    public static List<String> getRestAdminFields(String bucket, String table) {
        List<String> adminFields = (List<String>) cache.get(REST_ADMIN_FIELDS + bucket + "_" + table);
        if (null == adminFields) {
            List<RestAdmin> admins = getRestAdmin(bucket, table);
            adminFields = new ArrayList<String>();
            if (admins != null && admins.size() > 0) {
                for (RestAdmin rest : admins) {
                    adminFields.add(rest.getName());
                }
            }
            if (!adminFields.contains("id")) {
                adminFields.add("id");
            }
            if (adminFields != null && adminFields.size() > 0) {
                cache.put(REST_ADMIN_FIELDS + bucket + "_" + table, adminFields);
            }
        }
        return adminFields;
    }

    public static RestUser getRestUser(String bucket) {
        RestUser restUser = (RestUser) cache.get(REST_APP_USER_PREFIX + bucket);
        if (restUser == null) {
            IResterService resterService = WebApplicationContextUtil.getBean("resterService", IResterService.class);
            restUser = resterService.loadRestUser(bucket);
            if (restUser != null) {
                cache.put(REST_APP_USER_PREFIX + bucket, restUser);
            }
        }
        return restUser;
    }

    public static void clearRestUser(String bucket) {
        cache.remove(REST_APP_USER_PREFIX + bucket);
    }

    public static void setScriptEngine(String key, ScriptEngine value) {
        cache.put(REST_SCRIPT_ENGINE + key, value);
    }

    public static ScriptEngine getScriptEngine(String key) {
        return (ScriptEngine) cache.get(REST_SCRIPT_ENGINE + key);
    }

    public static void clearScriptEngine(String bucket, String name, String option, String version) {
        String key = "rester_" + bucket + "_" + name + "_" + option + "_" + version;
        cache.remove(REST_SCRIPT_ENGINE + key);
    }

    public static Object removeObject(String key) {
        return cache.remove(key);
    }

    public static void clear(String id) {
        Iterator<String> iter = cache.keySet().iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            if (key.indexOf(id) != -1) {
                cache.remove(key);
            }
        }
    }

    public static void clear() {
        cache.clear();
    }

}
